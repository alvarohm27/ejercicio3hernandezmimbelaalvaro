package main;

import java.util.Scanner;

public class Main {
	
	static Scanner in = new Scanner (System.in);

	public static void main(String[] args) {
		
		String cadena;
		
		System.out.println("Introduce una cadena de texto:");
		System.out.print("\n\tCadena = ");
		cadena=in.nextLine();
		
		System.out.println("\n\tCadena original = '" + cadena + "'.");
		
		cadena=cadena.toUpperCase();
		
		System.out.println("\n\tCadena en may�sculas = '" + cadena + "'.");
	}

}
